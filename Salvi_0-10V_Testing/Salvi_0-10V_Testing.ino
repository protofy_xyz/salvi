void setup() {

  pinMode(4, OUTPUT);
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);
  Serial.begin(115200);
  delay(500);

  for (int i = 0; i <= 100; i++) {
    analogWrite(4, 0);
    delay(100);
    Serial.print(0);
    Serial.print(" ");
    Serial.print(analogRead(A4));
    Serial.print(" ");
    Serial.println(analogRead(A5));
  }

  for (int a = 0; a <= 255; a += 20) {
    for (int i = 0; i <= 50; i++) {
      analogWrite(4, a);
      delay(100);
      Serial.print(a);
      Serial.print(" ");
      Serial.print(analogRead(A4));
      Serial.print(" ");
      Serial.println(analogRead(A5));
    }
  }

  for (int a = 0; a <= 1; a++) {
    for (int i = 0; i <= 255; i++) {
      analogWrite(4, i);
      delay(100);
      Serial.print(i);
      Serial.print(" ");
      Serial.print(analogRead(A4));
      Serial.print(" ");
      Serial.println(analogRead(A5));
    }
  }

  for (int a = 0; a <= 1; a++) {
    for (int i = 255; i >= 0; i--) {
      analogWrite(4, i);
      delay(100);
      Serial.print(i);
      Serial.print(" ");
      Serial.print(analogRead(A4));
      Serial.print(" ");
      Serial.println(analogRead(A5));
    }
  }

  for (int i = 0; i <= 100; i++) {
    analogWrite(4, 0);
    delay(100);
    Serial.print(0);
    Serial.print(" ");
    Serial.print(analogRead(A4));
    Serial.print(" ");
    Serial.println(analogRead(A5));
  }

  for (int i = 0; i <= 50; i++) {
    analogWrite(4, 255);
    delay(100);
    Serial.print(255);
    Serial.print(" ");
    Serial.print(analogRead(A4));
    Serial.print(" ");
    Serial.println(analogRead(A5));
  }

  for (int i = 0; i <= 50; i++) {
    analogWrite(4, 100);
    delay(100);
    Serial.print(100);
    Serial.print(" ");
    Serial.print(analogRead(A4));
    Serial.print(" ");
    Serial.println(analogRead(A5));
  }

  for (int i = 0; i <= 50; i++) {
    analogWrite(4, 255);
    delay(100);
    Serial.print(255);
    Serial.print(" ");
    Serial.print(analogRead(A4));
    Serial.print(" ");
    Serial.println(analogRead(A5));
  }

  for (int i = 0; i <= 100; i++) {
    analogWrite(4, 0);
    delay(100);
    Serial.print(0);
    Serial.print(" ");
    Serial.print(analogRead(A4));
    Serial.print(" ");
    Serial.println(analogRead(A5));
  }
}

void loop() {

}
