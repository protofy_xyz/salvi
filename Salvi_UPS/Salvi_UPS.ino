#include "UPS.h"

#define pinEnCharge   8
#define pinEnBattery  9
#define pinCharging   11
#define pinPower      10
#define pinMosfet     12
#define pinBattery    A1
#define pinBridge     A2
#define pinOutput     A3

#define check_period  10000
#define step_duration 60000
long    prev_check;
long    prev_step;

UPS UPS1 = UPS();

int state = 1;



void setup() {

  Serial.begin(115200);

  UPS1.configurePins(pinEnCharge, pinEnBattery, pinCharging, pinPower, pinMosfet, pinBattery, pinBridge, pinOutput);

}

void loop() {

  if (millis() >= prev_check + check_period) {
    prev_check = millis();

    Serial.print("Time: ");
    Serial.print(millis());

    Serial.print("  State: ");
    Serial.print(state);

    Serial.print("  enChg: ");
    Serial.print(UPS1.EN_CHARGE);

    Serial.print("  enBat: ");
    Serial.print(UPS1.EN_BATTERY);

    Serial.print("  Chg: ");
    Serial.print(UPS1.chargeStatus());

    Serial.print("  Pwr: ");
    Serial.print(UPS1.powerStatus());
    //
    //    Serial.print("  Mos: ");
    //    Serial.print(UPS1.MOSFET);

    Serial.print("  V_Bat: ");
    Serial.print(UPS1.batteryVoltage());

    Serial.print("  V_Brg: ");
    Serial.print(UPS1.bridgeVoltage());

    Serial.print("  V_Out: ");
    Serial.print(UPS1.outputVoltage());

    Serial.print("  V_in: ");
    Serial.print(analogRead(A4) * 0.0049);

    Serial.println();
  }

  switch (state) {

    case 1:
      UPS1.enCharge(true);
      UPS1.enBattery(true);
      UPS1.mosfet(false);

      if (!UPS1.powerStatus()) {
        //state = 2;
        prev_step = millis();
      }
      break;

    case 2:
      UPS1.enCharge(true);
      UPS1.enBattery(true);
      UPS1.mosfet(false);

      if (UPS1.powerStatus()){
        state = 1;
        prev_step = millis();
      }

      if (millis() >= prev_step + step_duration * 5) {
        state = 3;
        prev_step = millis();
      }
      break;

    case 3:
      UPS1.enCharge(true);
      UPS1.enBattery(false);
      UPS1.mosfet(false);

      if (UPS1.powerStatus()) {
        state = 1;
        prev_step = millis();
      }
      break;

    case 4:
      UPS1.enCharge(false);
      UPS1.enBattery(false);
      UPS1.mosfet(false);

      if (millis() >= prev_step + step_duration * 5) {
        state = 1;
        prev_step = millis();
      }
      break;

    case 5:
      UPS1.enCharge(true);
      UPS1.enBattery(true);
      UPS1.mosfet(true);

      if (millis() >= prev_step + step_duration) {
        state = 6;
        prev_step = millis();
      }
      break;

    case 6:
      UPS1.enCharge(false);
      UPS1.enBattery(true);
      UPS1.mosfet(true);

      if (millis() >= prev_step + step_duration) {
        state = 7;
        prev_step = millis();
      }
      break;

    case 7:
      UPS1.enCharge(true);
      UPS1.enBattery(false);
      UPS1.mosfet(true);

      if (millis() >= prev_step + step_duration) {
        state = 8;
        prev_step = millis();
      }
      break;

    case 8:
      UPS1.enCharge(false);
      UPS1.enBattery(false);
      UPS1.mosfet(true);

      if (millis() >= prev_step + step_duration) {
        state = 1;
        prev_step = millis();
      }
      break;
  }
}
