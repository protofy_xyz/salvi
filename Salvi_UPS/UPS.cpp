#include "UPS.h"

UPS::UPS() {
};

void UPS::configurePins(int pinEnCharge, int pinEnBattery, int pinCharging, int pinPower, int pinMosfet, int pinBattery, int pinBridge, int pinOutput) {

  _pinEnCharge = pinEnCharge;
  _pinEnBattery = pinEnBattery;
  _pinCharging = pinCharging;
  _pinPower = pinPower;
  _pinMosfet = pinMosfet;
  _pinBattery = pinBattery;
  _pinBridge = pinBridge;
  _pinOutput = pinOutput;

  pinMode(_pinEnCharge, OUTPUT);
  pinMode(_pinEnBattery, OUTPUT);
  pinMode(_pinCharging, INPUT);
  pinMode(_pinPower, INPUT);
  pinMode(_pinMosfet, OUTPUT);
  pinMode(_pinBattery, INPUT);
  pinMode(_pinOutput, INPUT);

}

void UPS::enCharge(bool _EN_CHARGE) {
  digitalWrite(_pinEnCharge, !_EN_CHARGE);
  EN_CHARGE = _EN_CHARGE;
}

void UPS::enBattery(bool _EN_BATTERY) {
  digitalWrite(_pinEnBattery, !_EN_BATTERY);
  EN_BATTERY = _EN_BATTERY;
}

bool UPS::chargeStatus() {
  CHARGING = !digitalRead(_pinCharging);
  return CHARGING;
}

bool UPS::powerStatus() {
  POWER = !digitalRead(_pinPower);
  return POWER;
}

void UPS::mosfet(bool _MOSFET) {
  digitalWrite(_pinMosfet, _MOSFET);
  MOSFET = _MOSFET;
}

float UPS::batteryVoltage() {
  batteryVolt = analogRead(_pinBattery) * 0.0049;
  return batteryVolt;
}

float UPS::bridgeVoltage() {
  bridgeVolt = analogRead(_pinBridge) * 0.0049;
  return bridgeVolt;
}

float UPS::outputVoltage() {
  outputVolt = analogRead(_pinOutput) * 0.0049;
  return outputVolt;
}
