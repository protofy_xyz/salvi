#include <Arduino.h>

class UPS {

  public:

    UPS();

    void configurePins(int pinEnCharge, int pinEnBattery, int pinCharging, int pinPower, int pinMosfet, int pinBattery, int pinBridge, int pinOutput);

    void enCharge(bool _EN_CHARGE);
    void enBattery(bool _EN_BATTERY);
    bool chargeStatus();
    bool powerStatus();
    void mosfet(bool _MOSFET);
    float batteryVoltage();
    float bridgeVoltage();
    float outputVoltage();

    bool EN_CHARGE;
    bool EN_BATTERY;
    bool CHARGING;
    bool POWER;
    bool MOSFET;
    float batteryVolt;
    float bridgeVolt;
    float outputVolt;

  private:

    int _pinEnCharge;
    int _pinEnBattery;
    int _pinCharging;
    int _pinPower;
    int _pinMosfet;
    int _pinBattery;
    int _pinBridge;
    int _pinOutput;

};
